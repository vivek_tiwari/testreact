import React from 'react';
import ReactDOM from 'react-dom';
import {render} from 'react-dom';
import HeaderBar from 'd2-ui/lib/header-bar/HeaderBar.component';
import Sidebar from 'd2-ui/lib/sidebar/Sidebar.component';
import FontIcon from 'material-ui/lib/font-icon';
import SideComponent from './SideComponent';



export default React.createClass({
  propTypes : {
    d2 : React.PropTypes.object.isRequired
  },
  childContextTypes: {
      d2: React.PropTypes.object,
  },
  getChildContext() {
      return {
          d2: this.props.d2,
      };
  },
  render(){
    return (
      <div>
      <HeaderBar/>
      <SideComponent/>
      </div>
    )
  }
});
