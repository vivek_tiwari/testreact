import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/AppComponent';

//Testing components

//Styling
require('d2-ui/scss/HeaderBar.scss'); //header-bar styling


//Connect d2
import { init } from 'd2/lib/d2';
// D2 UI

function renderExamples(d2Context){
  //console.log(d2Context);
}

jQuery.ajaxSetup({
    headers: {
        Authorization: 'Basic ' + btoa('admin:district'),
    },
});
init({baseUrl: 'http://localhost:8080/api'})
.then(
  d2 => {
    window.d2=d2;
    function renderApp() {
        ReactDOM.render(<App d2={d2} />, document.getElementById('app'));
    }
     //console.log(d2.models);
    // Promise.resolve(d2.models.organisationUnits.list()).then(
    //   organisationUnitList => {
    //     organisationUnitList.forEach(organisationUnit => console.log(organisationUnit.dataValues))
    //   }
    // )
    renderApp();
  }
);
