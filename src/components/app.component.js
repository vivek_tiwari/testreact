import React from 'react';
import log from 'loglevel';

//d2
import HeaderBar from 'd2-ui/lib/header-bar/HeaderBar.component';
import OrgUnitTree from 'd2-ui/lib/org-unit-tree';

//App
import HackyDropdown from './drop-down';
import AppTheme from '../theme';

class App extends React.Component {
    constructor(props,context){
        super(props);
        this.state = Object.assign({},{
            programList: [],
            selectedOrg: '',
            selectedProg: null,
            allOrgProgData: []
        });
        this.props = props;
    }

    getChildContext() {
        return {
            d2: this.props.d2,
            root: this.props.root,
        };
    }

    componentDidMount() {
    console.log("did mount");
    }

    componentWillUnmount() {
        console.log("will mount");
    }

    _handleOrgTreeClick(event, orgUnit) {
        this.setState(state => {
            if (state.selectedOrg === orgUnit.id) {
                return { selectedOrg: '' };
            }
            return { selectedOrg: orgUnit.id };
        });

        // fecthing all programs under that org-unit
        let dropdownProgList = [];
        this.props.d2.models.organisationUnits.get(orgUnit.id,{paging:false,fields:'id,name,programs[id,name,programStages[id,name,programStageDataElements[id,dataElement[id,name,optionSet[id,name,version]]]],organisationUnits,programTrackedEntityAttributes,trackedEntity]'})
        .then(orgUnitData => {
            this.setState({
                allOrgProgData: orgUnitData.programs
            });
            orgUnitData.programs.forEach(oneProgram => {
                dropdownProgList.push({id:oneProgram.id,displayName:oneProgram.name});
            })
            this.setState({
                programList: dropdownProgList
            });
        })
        .catch(err => {
            log.error('Failed to load Org programs',err);
        })
    };

    _handleDropdownChange(obj) {
        // TODO Hnadle following bugs :-
        // Information Campaign / Conraceptive Voucher program and some others give error : Uncaught TypeError: Cannot read property 'id' of undefined
            this.setState({
                selectedProg: obj.target.value
            },function(){
                let selectedProgData = this.state.allOrgProgData.valuesContainerMap.get(this.state.selectedProg);
            });
    }

    render() {

        const styles = {
            header: {
                fontSize: 24,
                fontWeight: 100,
                color: AppTheme.rawTheme.palette.textColor,
                padding: '6px 16px',
            },
            card: {
                marginTop: 8,
                marginRight: '1rem',
            },
            cardTitle: {
                background: AppTheme.rawTheme.palette.primary2Color,
                height: 62,
            },
            cardTitleText: {
                fontSize: 28,
                fontWeight: 100,
                color: AppTheme.rawTheme.palette.alternateTextColor,
            },
            forms: {
                minWidth: AppTheme.forms.minWidth,
                maxWidth: AppTheme.forms.maxWidth,
            },
            box: {
                position:'fixed',
                left:'0px',
                border: '1px solid #eaeaea',
                width: '300px',
                height: '100%',
                overflowY: 'hidden',
                backgroundColor: AppTheme.rawTheme.palette.accent2Color,
            },
            treeBox: {
                backgroundColor:'white',
                border: '1px solid #eaeaea',
                height: '400px',
                width:'100%',
                marginTop: '40px',
                overflowY:'auto',
                fontSize: 13,
           },
           parent: {
              position:'absolute',
              minHeight:'100%',
              height: 'auto',
              width: '100%',
              overflowX: 'hidden',
              display:'flex',
              left: '0px',
              backgroundColor: AppTheme.rawTheme.palette.canvasColor,
           },
           dropdown: {
                marginLeft: '10px',
                marginTop: '20px',
                width: 350
            },
        };

        return (
            <div className="app-wrapper" style={styles.parent}>
                <HeaderBar />
                <div style={styles.box}>
                    <div style={styles.treeBox}>
                        <OrgUnitTree
                              root={this.props.root}
                              onClick={this._handleOrgTreeClick.bind(this)}
                              selected={this.state.selectedOrg}
                          />
                    </div>
                </div>

                <div className="content-area" style={styles.forms}>
                    <div style={styles.header}>
                         <p>Tracker Capture Entry App</p>
                    </div>
                    <HackyDropdown value='dropValue' onChange={this._handleDropdownChange.bind(this)} menuItems={this.state.programList} includeEmpty={true} emptyLabel='Select Program' />
                </div>


            </div>
        );
    }
}

App.propTypes = { d2: React.PropTypes.object, root: React.PropTypes.any };
App.childContextTypes = { d2: React.PropTypes.object, root: React.PropTypes.any };

export default App;
