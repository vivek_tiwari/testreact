var HtmlWebpackPlugin = require('html-webpack-plugin');
var path = require('path');
require('es6-promise').polyfill()

var HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template : __dirname + '/app/index.html',
  filename : 'index.html',
  inject : 'body'
});

module.exports = {
  entry : [
    './app/index.js'
  ],
  output : {
    path : __dirname + '/dist',
    filename : "index_bundle.js"
  },
  module : {
    loaders : [
      {
          test :/\.js$/, exclude : /node_modules/,
          loader : "babel-loader"
      },
      {
          test: /\.css$/,
          loader: 'style-loader!css-loader',
      },
      {
          test: /\.scss$/,
          loader: 'style!css!sass',
      }
    ]
  },
  plugins : [HtmlWebpackPluginConfig]
};
