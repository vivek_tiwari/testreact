# README #

### What is this repository for? ###

* A simple react app testing basic d2-ui and d2 components.
* Version-None

### How do I get set up? ###

* First clone the repo to your local machine.
* Install dependencies next with 'npm install'
* Run development server using 'npm start'

### Contribution guidelines ###

* Currently none

### Who do I talk to? ###

* Repo owner