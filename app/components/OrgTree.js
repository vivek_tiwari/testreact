import React from 'react';
import { render } from 'react-dom';
import log from 'loglevel';
import { Card, CardText } from 'material-ui/lib/card';
import { init } from 'd2/lib/d2';
import OrgUnitTree from 'd2-ui/lib/org-unit-tree';

const el = document.getElementById('wtf');
let d2 = null;
const styles = {
    card: {
        margin: 16,
        width: 350,
        float: 'left',
        transition: 'all 175ms ease-out',
    },
    cardText: {
        paddingTop: 0,
        color:'black !important'
    },
    cardHeader: {
        borderBottom: '1px solid #eeeeee',
    },
    customLabel: {
        fontStyle: 'italic',
        fontSize:'12px'
    },
    customLabelSelected: {
        color: 'blue',
        weight: 900,
    },
};

function OrgUnitTreeExample(props) {
    //get Org-Unit-Tree
    return (
        <div>
                <h6 style={styles.cardHeader}>OrgUnitTree Listing</h6>
                <OrgUnitTree root={props.root}
                labelStyle={styles.customLabel}/>
        </div>
    );
}
OrgUnitTreeExample.propTypes = { root: React.PropTypes.any, roots: React.PropTypes.any, preRoot: React.PropTypes.any };


function loadUnits(props){
  console.log(props);
  let d2=props.d2;
  return (
  d2.models.organisationUnits.list({ paging: false, level: 1, fields: 'id,displayName,children::isNotEmpty' })
    .then(rootLevel => rootLevel.toArray()[0] )
    .then(rootUnit => {
      return <OrgUnitTreeExample root={rootUnit} roots={[]} />
      //above code rendered all the units of passed root.
    })
  )
}
module.exports = loadUnits;
