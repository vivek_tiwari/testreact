import React from 'react';
import Sidebar from 'd2-ui/lib/sidebar/Sidebar.component';
import FontIcon from 'material-ui/lib/font-icon';
import OrgTree from './OrgTree';


//Custom Css styling for layout.
const styles = {
    box: {
        backgroundColor:'#f3f3f3',
        position: 'absolute',
        border: '1px solid #eaeaea',
        width: 'auto',
        height: '100%',
        float: 'left',
    },
    header: {
        marginTop : '50px',
        backgroundColor: '#f3f3f3',
        color: 'grey',
    },
    headerText: {
    },
    leftBar: {
      marginTop:'40px'
    },
    page: {
        backgroundColor:'white',
        border: '1px solid #eaeaea',
        height: '400px',
        overflowY:'auto'
    },
    iconStyles: {
      paddingLeft:'5px',
      float: 'right',
      fontSize: '16px',
      fontWeight: '900',
      color: '#666',
      cursor:'pointer'
    },
    myClass:{
      color:'white',
      background:'grey',
      height:'300px',
      width:'200px',
    }
};

//My Menu Items
const sections = [
    { key: 's1', label: 'Registration and Data Entry' },
    { key: 's2', label: 'Reports' },
];

const sectionAction = {
  's1':{'visible':true},
  's2':{'visible':false}
}

//My handler for click on any section aka menu-item.
function changeSectionHandler(key, searchText) {
    //do nothing lol
}

//I wanna show some buttons at top of my SidebarExample
function getButtons(){
  return (
    <div>
    <FontIcon className="material-icons" style={styles.iconStyles}>arrow_forward</FontIcon>
    <FontIcon className="material-icons" style={styles.iconStyles}>arrow_back</FontIcon>
    <FontIcon className="material-icons" style={styles.iconStyles}>home</FontIcon>
    </div>
  )
}

var SideComponent =  React.createClass({
  changeSectionHandler: function(key,searchText){
    this.setState({
      currentSection : key
    })
  },
  componentDidMount:function(){
    Promise.resolve(OrgTree(this.context)).then(
      resultComponent => {
        this.setState({
          displaySection:resultComponent
        })
      }
    )
  },
  getInitialState: function(){
    return ({
    currentSection : sections[0].key,
    displaySection : "null"
    }
    )
  },
  contextTypes: {
      d2: React.PropTypes.object.isRequired,
  },
  render(){

//    Promise.resolve(true).then(resp => { this.setState({displaySection:'what'})});
    return (
      <div style={styles.box}>
          <div style={styles.leftBar}>
              <Sidebar
                  sections={sections}
                  onChangeSection={this.changeSectionHandler}
                  sideBarButtons={getButtons()}

              />
          </div>

          {renderSection(this.state.currentSection,this.state.displaySection)}
      </div>
    )
  }
});

module.exports = SideComponent;

function renderSection(section,content){
  return sectionAction[section].visible && (
    <div style={styles.page}>
    {content}
    </div>
  )
}
